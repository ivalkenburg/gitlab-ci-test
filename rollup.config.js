import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';
import {terser} from 'rollup-plugin-terser';

export default {
    input: 'src/main.js',
    output: {
        file: 'dist/gitlab-ci-test.min.js',
        format: 'umd',
        name: 'GitlabCiTest',
    },
    plugins: [
        commonjs(),
        nodeResolve({browser: true, preferBuiltins: true}),
        babel({compact: true}),
        terser(),
    ],
};
