import _template from 'lodash/template';

export default function (template, values) {
    return _template(template)(values);
}
